package br.edu.fanor.service;

import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import br.edu.fanor.dao.SolicitacaoDAO;
import br.edu.fanor.entity.Professor;
import br.edu.fanor.entity.Reserva;
import br.edu.fanor.entity.Sala;
import br.edu.fanor.entity.Solicitacao;
import br.edu.fanor.entity.Usuario;

@Stateless
public class SolicitacaoService extends GenericService<Solicitacao>{

	private static final long serialVersionUID = -8469735845097835531L;

	@EJB
	SolicitacaoDAO solicitacaoDAO;
	
	public void salvaSolicitacao(Solicitacao solicitacao) {
		solicitacaoDAO.insert(solicitacao);
	}

	public List<Solicitacao> listSolicitacaoProf(Long id){
		List<Solicitacao> list = solicitacaoDAO.findAllProf(id);
		return list;
	}
	
	public List<Solicitacao> listSolicitacaoPendente(){
		List<Solicitacao> list = solicitacaoDAO.findSolicitacoesPendentes();
		return list;
	}

	public List<Reserva> listSolicitacaoDoDia() {
		List<Reserva> list = solicitacaoDAO.findSolicitacoesDoDia();
		return list;
	}
	
	public List<Solicitacao> pesquisaSolicitacao(Date data) {
		List<Solicitacao> solicitacoes = solicitacaoDAO.findSolicitacaoFromDate(data);
		return solicitacoes;
	}

	public List<Solicitacao> pesquisaSolicitacao(Date data, String nome) {
		if(nome=="" || nome==null)
			return pesquisaSolicitacao(data);
		else{
		List<Solicitacao> solicitacoes = solicitacaoDAO.findSolicitacaoFromDate(data,nome);
		
		return solicitacoes;
		}
	}
	
}