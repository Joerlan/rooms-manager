package br.edu.fanor.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import br.edu.fanor.entity.Professor;
import br.edu.fanor.entity.Reserva;
import br.edu.fanor.entity.Sala;
import br.edu.fanor.entity.Solicitacao;
import br.edu.fanor.entity.Usuario;

@Stateless
public class SolicitacaoDAO extends GenericDAO<Solicitacao>{

	private static final long serialVersionUID = -2937516627590134125L;

	@SuppressWarnings("unchecked")
	public List<Solicitacao> findAllProf(Long id) {
		List<Solicitacao> list = new ArrayList<Solicitacao>();
		Query query = getEntityManager().createQuery("from solicitacoes");
		list = query.getResultList();
		return list;
	}

	@SuppressWarnings("unchecked")
	public List<Solicitacao> findSolicitacoesPendentes(){
		List<Solicitacao> list = new ArrayList<Solicitacao>();
		Query query = getEntityManager().createQuery("from solicitacoes s where s.id not in (select r.solicitacao.id from reservas r where r.solicitacao.id = s.id)");
		list = query.getResultList();
		return list;
	}

	@SuppressWarnings("unchecked")
	public List<Reserva> findSolicitacoesDoDia() {
		List<Reserva> list = new ArrayList<Reserva>();
		Query query = getEntityManager().createQuery("from reservas");
		list = query.getResultList();
		
		
		return list;
	}
	
	@SuppressWarnings("unchecked")
	public List<Solicitacao> findSolicitacaoFromDate(Date data) {
		Date dataFinal = new Date(data.getTime()+TimeUnit.DAYS.toMillis(1));
		
		Criteria criteria = getCriteria(Solicitacao.class);
		criteria.add(Restrictions.between("data", data, dataFinal));//("data", dataFinal))
		return criteria.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<Solicitacao> findSolicitacaoFromDate(Date data, String nome) {
		Criteria criteria = getCriteria(Solicitacao.class);
		if(nome=="" || nome ==null)
			return findSolicitacaoFromDate(data);
		else{
		Date dataFinal = new Date(data.getTime()+TimeUnit.DAYS.toMillis(1));
	
		criteria.createAlias("professor", "p");
		criteria.add(Restrictions.between("data", data, dataFinal));
		criteria.add(Restrictions.ilike("p.nome",nome,MatchMode.ANYWHERE));
		return criteria.list();
		}
		
	
	}
	
	
	
	
}
