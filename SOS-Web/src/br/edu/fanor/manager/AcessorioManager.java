package br.edu.fanor.manager;

import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import br.edu.fanor.entity.Acessorio;
import br.edu.fanor.entity.Usuario;
import br.edu.fanor.exceptions.ValidacaoException;
import br.edu.fanor.service.AcessorioService;

@SessionScoped
@ManagedBean(name = "acessorioManager")
public class AcessorioManager extends AbstractMB implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6830205910133853851L;

	@EJB
	AcessorioService acessorioService;

	private List<Acessorio> listAcessorios;

	private Acessorio acessorio = new Acessorio();
	private Map<String, Acessorio> mapListAcessorios = new HashMap<String, Acessorio>();

	public List<Acessorio> getListAcessorios() {
		listAcessorios = acessorioService.listaTodos();
		return listAcessorios;
	}

	public void setListAcessorios(List<Acessorio> listAcessorios) {
		this.listAcessorios = listAcessorios;
	}

	public Acessorio getAcessorio() {
		if (acessorio == null) {
			acessorio = new Acessorio();
		}
		return acessorio;
	}

	public void setAcessorio(Acessorio acessorio) {
		this.acessorio = acessorio;
	}

	public Map<String, Acessorio> getMapListAcessorios() {
		this.mapListAcessorios = acessorioService.getMapListAcessorios();
		return mapListAcessorios;
	}

	public void setMapListAcessorios(Map<String, Acessorio> mapListAcessorios) {
		this.mapListAcessorios = mapListAcessorios;
	}

	public String pegar(Acessorio acessorio) {
		setAcessorio(acessorio);
		return "cadastroAcessorio";
	}

	public void salvar() throws IOException {
		try {
			acessorioService.saveOrUpdate(acessorio);
			FacesContext.getCurrentInstance().getExternalContext()
					.redirect("/SOS-Web/paginas/admin/listaAcessorio.jsf");
		} catch (ValidacaoException e) {
			displayErrorMessageToUser("Não foi possivel salvar o usuário, verifique os dados informados ou tente mais tarde.");
		}

	}

	public void deletar(Acessorio acessorio) {
		try {
			acessorioService.delete(acessorio);
			;
			displayInfoMessageToUser("Usuário " + acessorio.getNome()
					+ " excluido com sucesso.");
		} catch (Exception e) {
			displayErrorMessageToUser("Não foi possivel excluir o usuário");
		}

	}

	public String cadastroAcessorio() {
		acessorio = null;
		return "cadastroAcessorio";
	}

}
