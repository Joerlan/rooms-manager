package br.edu.fanor.manager;

import br.edu.fanor.entity.Reserva;
import br.edu.fanor.entity.Solicitacao;
import br.edu.fanor.entity.Usuario;
import br.edu.fanor.manager.Teste;
import br.edu.fanor.service.SolicitacaoService;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;

import org.primefaces.component.calendar.Calendar;

@SuppressWarnings("unused")
@RequestScoped
@ManagedBean
public class SolicitacaoManager {
	
	@EJB private SolicitacaoService solicitacaoService;
	
	private List<Solicitacao> listaSolicitacaoPendente = new ArrayList<Solicitacao>();
	private List<Reserva> listaSolicitacaoDoDia = new ArrayList<Reserva>();
	private List<Reserva> listaReservados = new ArrayList<Reserva>();
	private List<Solicitacao> listSolicitacaoData = new ArrayList<Solicitacao>();
	
	//se nao for necessario apagar ivonildo
	private Date data;
	private String responsavel;
	
	

	public String getResponsavel() {
		return responsavel;
	}

	public void setResponsavel(String responsavel) {
		this.responsavel = responsavel;
	}


	
	

	public List<Solicitacao> getListSolicitacaoData() {
		
		return listSolicitacaoData;
	}

	public void setListSolicitacaoData(List<Solicitacao> listSolicitacaoData) {
		this.listSolicitacaoData = listSolicitacaoData;
	}
	
	

	// apagar pra cima
	
	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	//fim do meus get e set incluindos
	public List<Solicitacao> getListaSolicitacaoPendente(){
		List<Solicitacao> list = solicitacaoService.listSolicitacaoPendente();
		return list;
	}
	
	public void setListaSolicitacaoPendente(List<Solicitacao> listaSolicitacaoPendente) {
		this.listaSolicitacaoPendente = listaSolicitacaoPendente;
	}

	public SolicitacaoService getSolicitacaoService() {
		return solicitacaoService;
	}

	public void setSolicitacaoService(SolicitacaoService solicitacaoService) {
		this.solicitacaoService = solicitacaoService;
	}

	public void validarHora(FacesContext context, UIComponent component, Object value) throws ValidatorException {
	    
		Date inicio = (Date) ((Calendar) component.findComponent("inicio")).getValue();
		Date fim = (Date) value;
		
		
        if(inicio.after(fim)) {
            FacesMessage message = new FacesMessage();	
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            message.setSummary("Time Error.");
            message.setDetail("A hora inicial tem que ser anterior a hora final");
            context.addMessage("formSolicitacao:inicio", message);
            throw new ValidatorException(message);
        }
    
	}

	public List<Reserva> getListaReservados() {
		setListaReservados(solicitacaoService.listSolicitacaoDoDia());
		return listaReservados;
	}

	public void setListaReservados(List<Reserva> listaReservados) {
		this.listaReservados = listaReservados;
	}
	
	
	public List<Reserva> getListaSolicitacaoDoDia() {
		setListaSolicitacaoDoDia(solicitacaoService.listSolicitacaoDoDia());
		return listaSolicitacaoDoDia;
	}

	public void setListaSolicitacaoDoDia(List<Reserva> listaSolicitacaoDoDia) {
		this.listaSolicitacaoDoDia = listaSolicitacaoDoDia;
	}
	
	public List<Solicitacao> listaSolicitacaoPorData(){
		return listSolicitacaoData = solicitacaoService.pesquisaSolicitacao(getData());
	}

	public List<Solicitacao> listaSolicitacaoPorDataNome(){
		String nome = getResponsavel();
		
		if(!nome.equals("") || nome != null)
		return listSolicitacaoData = solicitacaoService.pesquisaSolicitacao(getData(),nome);
		else 
			return listaSolicitacaoPorData();
	}

	
}